package com.example.aninterface

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private lateinit var email:EditText
    private lateinit var password:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toastBtn = findViewById<Button>(R.id.register)

        val email = findViewById<EditText>(R.id.email)
        val password = findViewById<EditText>(R.id.password)
        val confirmPassword = findViewById<EditText>(R.id.confirmPassword)
        val passwordLength = findViewById<EditText>(R.id.password)
        val passwordLength2 = findViewById<EditText>(R.id.confirmPassword)
init()


        register.setOnClickListener {


            if (email.text.contains(" ")) {
                Toast.makeText(this, "email invalid", Toast.LENGTH_SHORT).show()
            }

            if (!email.text.contains("@")) {
                Toast.makeText(this, "email invalid", Toast.LENGTH_SHORT).show()
            }
            if (!email.text.contains("gmail.com")) {
                Toast.makeText(this, "email invalid", Toast.LENGTH_SHORT).show()
            }
            if (password.text.isEmpty()) {
                Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show()
            }


            if (passwordLength.text.length < 9) {
                Toast.makeText(this, "password is too short", Toast.LENGTH_SHORT).show()
            }
            if (password.equals(confirmPassword)) {
                Toast.makeText(this, "password matches", Toast.LENGTH_SHORT).show()
            }
            if (password.equals(confirmPassword) || email.text.contains(".") || email.text.contains(
                    "@"
                ) || password.text.length > 9
            ) {
                Toast.makeText(this, "user has been registered", Toast.LENGTH_SHORT).show()
            }
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email.toString(), password.toString())
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        gotoprofile()

                    } else {
                        Toast.makeText(this, "could not register", Toast.LENGTH_SHORT).show()
                    }
                }


        }


    }

    private fun gotoprofile() {
        startActivity(Intent(this, successActivity::class.java))
        finish()
    }
    private fun init(){
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)

    }

}